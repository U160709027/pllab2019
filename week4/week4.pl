use strict;
use warnings;

my $x = "This is a string";
print $x, "\n";

$x = 6;
print $x, "\n";

my @array = ("Ali", 6, 7, 8);	#their types are scalar
print @array, "\n";
print $array[1], "\n";

my @array2 = ([1,2,3], [4,5,6], [7,8,9]);
print $array2[2][1], "\n";
print @{$array2[1]}, "\n";

my %grades = ("Ali" => 50, "Mehmet" => 75, "Ayşe" => 80);
print $grades{"Mehmet"}, "\n";


my @array3 = ('one', 'two', 'three');
my $a = @array3;	#scalar context for assignment, return size
print $a, "\n";		#3

my ($b) = @array3;	#list context for assignment
print $b, "\n";		#one

my ($b, $c, $d) = @array3;
print '$b$c$d', "\n";	#$b$c$d
print "$b$c$d", "\n";	#onetwothree

print $b.$c.$d, "\n";	#onetwothree #concatenation operator

