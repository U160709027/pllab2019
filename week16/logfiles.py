class HashMap:
    def __init__(self):
        self.size = 16 #size of array
        self.map = [None] * self.size #our array that we're going to store the data

    def _get_hash(self, key):
        hash = 0
        for char in str(key):
            hash += ord(char)
        return hash % self.size #returning index

    def add(self, key, value):
        key_hash = self._get_hash(key)
        key_value = [key, value]

        if self.map[key_hash] is None:
            self.map[key_hash] = list([key_value])
            return True
        else:
            for pair in self.map[key_hash]:
                if pair[0] == key:
                    pair[1] = value
                    return True
            self.map[key_hash].append(key_value)
            return True





    def keys(self):
        arr = []
        for i in range(0, len(self.map)):
            if self.map[i]:
                arr.append(self.map[i][0])
        return arr




h = HashMap()

h.add('07/Mar/2004:16:05:49','64.242.88.10')
h.add('07/Mar/2004:16:07:19','64.242.88.10')
h.add('07/Mar/2004:16:08:11','64.242.88.10')
h.add('07/Mar/2004:16:05:33','64.242.88.10')
h.add('07/Mar/2004:16:11:33','64.242.88.10')
h.add('07/Mar/2004:16:15:33','64.242.88.10')



print(h.keys())
